package componenet;

import data.MainPageLessonsData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CoursesComponent extends AbsComponent<CoursesComponent> {
  public CoursesComponent(WebDriver driver) {
    super(driver);
  }

  @FindBy(css = "[class='lessons'] .lessons__new-item_hovered")
  private List<WebElement> listOfCourses;

  public CoursesComponent filterCoursesByName(MainPageLessonsData coursesName) {
    Predicate<? super WebElement> predicate = x -> x.getText().trim().contains(coursesName.getName().trim());
    List<WebElement> selectedCourse = listOfCourses.stream().filter(predicate).collect(Collectors.toList());
    if (!selectedCourse.isEmpty()) {
      selectedCourse.get(0).click();
    }
    return this;
  }


  @FindBy(css = "[class='lessons'] .lessons__new-item-start, [class='lessons'] .lessons__new-item-bottom>.lessons__new-item-time")
  private List<WebElement> lessonsItem;


  public CoursesComponent getEarlierLastCourseItem(BinaryOperator<AbstractMap.SimpleEntry<Date, WebElement>> compareFunction) {
    HashMap<String, String> convertMonths = new HashMap<>();
    convertMonths.put("января", "/01");
    convertMonths.put("февраля", "/02");
    convertMonths.put("марта", "/03");
    convertMonths.put("апреля", "/04");
    convertMonths.put("мая", "/05");
    convertMonths.put("июня", "/06");
    convertMonths.put("июля", "/07");
    convertMonths.put("августа", "/08");
    convertMonths.put("сентября", "/09");
    convertMonths.put("октября", "/10");
    convertMonths.put("ноября", "/11");
    convertMonths.put("декабря", "/12");

    Optional<WebElement> earliestElement = lessonsItem.stream()
            .filter(element -> !element.getText().equals("О дате старта будет объявлено позже"))
            .map(element -> {
              String str = element.getText();
              Pattern pattern = Pattern.compile("(\\d+)\\s+([а-я]+).*");
              Matcher matcher = pattern.matcher(str);
              if (matcher.find()) {
                String day = matcher.group(1);
                String month = matcher.group(2);
                String thisYear = Year.now().toString();
                String date = day + convertMonths.get(month) + "/" + thisYear;
                try {
                  Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(date);
                  return new AbstractMap.SimpleEntry<>(date1, element);
                } catch (ParseException e) {
                  return null;
                }
              }
              return null;
            })
            .reduce(compareFunction)
            .map(AbstractMap.SimpleEntry::getValue);
    earliestElement.ifPresent(WebElement::click);
    return this;
  }

  public static AbstractMap.SimpleEntry<Date, WebElement> findEarliestCourse(AbstractMap.SimpleEntry<Date, WebElement> entry1, AbstractMap.SimpleEntry<Date, WebElement> entry2) {
    if (entry1.getKey().before(entry2.getKey())) {
      return entry1;
    } else {
      return entry2;
    }
  }

  public static AbstractMap.SimpleEntry<Date, WebElement> findLatestCourse(AbstractMap.SimpleEntry<Date, WebElement> entry1, AbstractMap.SimpleEntry<Date, WebElement> entry2) {
    if (entry1.getKey().after(entry2.getKey())) {
      return entry1;
    } else {
      return entry2;
    }
  }


}
