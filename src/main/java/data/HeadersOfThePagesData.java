package data;

public enum HeadersOfThePagesData {
  MainPageHeader("Авторские онлайн‑курсы для профессионалов"),
  ApacheKafkaCoursePageHeader("Практика по работе с Kafka для инженеров данных и разработчиков"),
  SystemAnalystCoursePageHeader("Системный аналитик. Advanced"),
  JavaSpecializationCourseHeader("Специализация Java-разработчик"),
  AndroidSpecializationCourseHeader("Android Developer"),
  QAAutomationSpecializationCourseHeader("QA Automation\n" + "Engineer"),
  FullstackDeveloperSpecializationCourseHeader("Fullstack\n" + "Developer"),
  CPlusSpecializationCourseHeader("C++ DEVELOPER"),
  CSharpSpecializationCourseHeader("C# Developer"),
  AdministratorLinuxSpecializationCourseHeader("Administrator Linux"),
  SystemAnalystSpecializationCourseHeader("Системный аналитик"),
  PythonSpecializationCourseHeader("Python Developer"),
  MachineLearningSpecializationCourseHeader("Machine Learning"),
  IOSSpecializationCourseHeader("iOS Developer"),
  PHPSpecializationCourseHeader(""),
  NetworkEngineerSpecializationCourseHeader("NetworkEngineerSpecialization"),
  ITRecruiterCourseHeader("IT-Recruiter"),
  AndroidDeveloperBasicCourseHeader("Разработчик Android. Базовый уровень"),
  AndroidDeveloperSpecializationCourseHeader("Android Developer"),
  PostgresSQLHeader("PostgreSQL для администраторов баз данных и разработчиков");


  private final String name;

  HeadersOfThePagesData(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}