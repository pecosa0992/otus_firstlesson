package pages;

import annotations.Path;
import data.MainPageLessonsData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@Path("/")
public class MainPage extends BasePage<MainPage> {

  public MainPage(WebDriver driver) {
    super(driver);
  }

  private final String lessonsLocator = "//div[contains(text(),'%s')]/ancestor::a";

  public MainPage moveToTheCourse(MainPageLessonsData mainPageLessonsData) {
    String selector = String.format(lessonsLocator, mainPageLessonsData.getName());
    this.actions.moveToElement($(By.xpath(selector))).click().build().perform();
    return this;
  }


}
