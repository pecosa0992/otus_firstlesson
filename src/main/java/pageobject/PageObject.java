package pageobject;

import data.HeadersOfThePagesData;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public abstract class PageObject<T> {
  protected WebDriver driver;
  protected Actions actions;
  protected JavascriptExecutor js;


  public PageObject(WebDriver driver) {
    this.driver = driver;
    this.actions = new Actions(driver);
    this.js = (JavascriptExecutor) driver;
    PageFactory.initElements(driver, this);
  }

  @FindBy(tagName = "h1")
  private WebElement header;

  public T headerShouldBeTheSameAs(HeadersOfThePagesData header) {
    Assertions.assertEquals(header.getName(), this.header.getText());
    return (T) this;
  }


  public WebElement $(By locator) {
    return driver.findElement(locator);
  }
}

