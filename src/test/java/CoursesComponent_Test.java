import annotations.Driver;
import componenet.CoursesComponent;
import data.HeadersOfThePagesData;
import data.MainPageLessonsData;
import extentions.UIExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import pages.MainPage;

@ExtendWith(UIExtension.class)
public class CoursesComponent_Test {
  @Driver
  private WebDriver driver;

  @Test
  public void checkFilterCoursesByName() {
    new MainPage(driver)
            .open()
            .headerShouldBeTheSameAs(HeadersOfThePagesData.MainPageHeader);
    new CoursesComponent(driver)
            .filterCoursesByName(MainPageLessonsData.QAAutomationSpecialization)
            .headerShouldBeTheSameAs(HeadersOfThePagesData.QAAutomationSpecializationCourseHeader);
  }

  @Test
  public void checkClickEarliestCourse() {
    new MainPage(driver)
            .open()
            .headerShouldBeTheSameAs(HeadersOfThePagesData.MainPageHeader);
    new CoursesComponent(driver)
            .getEarlierLastCourseItem(CoursesComponent::findEarliestCourse)
            .headerShouldBeTheSameAs(HeadersOfThePagesData.PostgresSQLHeader);
  }

  @Test
  public void checkClickLatestCourse() {
    new MainPage(driver)
            .open()
            .headerShouldBeTheSameAs(HeadersOfThePagesData.MainPageHeader);
    new CoursesComponent(driver)
            .getEarlierLastCourseItem(CoursesComponent::findLatestCourse)
            .headerShouldBeTheSameAs(HeadersOfThePagesData.CSharpSpecializationCourseHeader);
  }


}
