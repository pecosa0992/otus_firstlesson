import annotations.Driver;
import data.HeadersOfThePagesData;
import data.MainPageLessonsData;
import extentions.UIExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import pages.MainPage;

@ExtendWith(UIExtension.class)
public class Menu_Test {
  @Driver
  private WebDriver driver;


  @Test
  public void checkMoveToSelectedCourse() {
    new MainPage(driver)
            .open()
            .moveToTheCourse(MainPageLessonsData.QAAutomationSpecialization)
            .headerShouldBeTheSameAs(HeadersOfThePagesData.QAAutomationSpecializationCourseHeader);
  }

}
